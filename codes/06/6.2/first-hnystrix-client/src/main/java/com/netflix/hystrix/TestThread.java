package com.netflix.hystrix;

import com.netflix.hystrix.HystrixThreadPool.Factory;
import com.netflix.hystrix.HystrixThreadPool.HystrixThreadPoolDefault;
import com.netflix.hystrix.crazyit.run.ThreadCommandA;
import com.netflix.hystrix.crazyit.run.ThreadCommandB;
import com.netflix.hystrix.crazyit.run.ThreadCommandC;

public class TestThread {

	public static void main(String[] args) throws Exception {
		ThreadCommandA c1 = new ThreadCommandA();
		ThreadCommandB c2 = new ThreadCommandB();
		ThreadCommandC c3 = new ThreadCommandC();
		
		// 输出全局线程池数量
		System.out.println("全局线程池数量：" + Factory.threadPools.size());
		
		
		// ThreadCommandA 命令同步执行
		c1.execute();
		HystrixThreadPoolDefault pool = (HystrixThreadPoolDefault)c1.threadPool;
		// ThreadCommandB 异步执行 
		c2.queue();
		HystrixThreadPoolDefault pool2 = (HystrixThreadPoolDefault)c2.threadPool;
		// 输出两个命令的线程池执行情况
		System.out.println(pool.getExecutor().getCompletedTaskCount() + "---" + pool2.getExecutor().getCompletedTaskCount());
	}

}
