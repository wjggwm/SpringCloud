package com.netflix.hystrix;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.netflix.hystrix.HystrixCommand.Setter;

public class ErrorCommand extends HystrixCommand<String> {

	public ErrorCommand() {
	    super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("ExampleGroup"))
	            .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
	            		.withExecutionTimeoutInMilliseconds(2000))
	            .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
	            		.withCircuitBreakerRequestVolumeThreshold(10)));
	}
	
	protected String run() throws Exception {
		Thread.sleep(500);
		System.out.println("执行命令 ==========");
		throw new RuntimeException("alowys error");
	}

	@Override
	protected String getFallback() {
		// 调用服务失败，回退的方法
		return "error";
	}


	
}
