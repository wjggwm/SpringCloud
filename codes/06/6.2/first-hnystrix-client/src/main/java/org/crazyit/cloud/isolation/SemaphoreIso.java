package org.crazyit.cloud.isolation;

import java.util.concurrent.ThreadPoolExecutor;

import com.netflix.config.ConfigurationManager;
import com.netflix.hystrix.HystrixCircuitBreaker;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.HystrixCommandProperties.ExecutionIsolationStrategy;

/**
 * 信号量隔离策略测试类
 * 
 * @author 杨恩雄
 *
 */
public class SemaphoreIso {

	public static void main(String[] args) throws Exception {
		// 配置使用信号量的策略进行隔离
		ConfigurationManager.getConfigInstance().setProperty(
				"hystrix.command.default.execution.isolation.strategy",
				ExecutionIsolationStrategy.SEMAPHORE);
		// 设置最大并发数，默认值为10
		ConfigurationManager
				.getConfigInstance()
				.setProperty(
						"hystrix.command.default.execution.isolation.semaphore.maxConcurrentRequests",
						2);
		// 设置执行回退方法的最大并发，默认值为10
		ConfigurationManager
				.getConfigInstance()
				.setProperty(
						"hystrix.command.default.fallback.isolation.semaphore.maxConcurrentRequests",
						20);
		for (int i = 0; i < 6; i++) {
			final int index = i;
			Thread t = new Thread() {
				public void run() {
					MyCommand c = new MyCommand(index);
					c.execute();
				}
			};
			t.start();
		}
		Thread.sleep(5000);
	}
}
