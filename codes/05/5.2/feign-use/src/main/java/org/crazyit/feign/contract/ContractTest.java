package org.crazyit.feign.contract;

import org.crazyit.feign.MyFeignClient;
import org.crazyit.feign.PersonClient;

import feign.Feign;
import feign.gson.GsonEncoder;

public class ContractTest {

	public static void main(String[] args) {
		// 获取服务接口
		HelloClient helloClient = Feign.builder()
				.contract(new MyContract())
				.target(HelloClient.class, "http://localhost:8080/");
		// 请求Hello World接口
		String result = helloClient.myHello();
		System.out.println("    接口响应内容：" + result);
	}
}
