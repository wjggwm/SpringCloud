package org.crazyit.feign;

import java.lang.reflect.Type;

import feign.RequestTemplate;
import feign.codec.EncodeException;
import feign.codec.Encoder;

public class MyEncoder implements Encoder {

	public void encode(Object object, Type bodyType, RequestTemplate template)
			throws EncodeException {
		// 实现自己的Encode逻辑
	}
}
